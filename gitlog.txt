commit 2e9d05f659309c7734699a1c4d092581dfd54496
Merge: 759f78c d45d73a
Author: Floyd Butselaar <475474@student.saxion.nl>
Date:   Tue Feb 21 09:49:33 2023 +0000

    Merge branch '2-unit-testing' into 'main'
    
    added the greatest unit test in the world
    
    Closes #2
    
    See merge request F.Butselaar/devops-blank-project!5

commit d45d73a0f16cd4bd312eaf45854cf31001e8d001
Author: Floyd Butselaar <475474@student.saxion.nl>
Date:   Tue Feb 21 10:38:39 2023 +0100

    added the greatest unit test in the world

commit 759f78c771960e7d4827d78a0f73fdc145341d70
Merge: 6c29e27 3a2ec51
Author: Floyd Butselaar <475474@student.saxion.nl>
Date:   Thu Feb 16 15:12:17 2023 +0000

    Merge branch 'feature_branch' into 'main'
    
    Changed link
    
    See merge request F.Butselaar/devops-blank-project!4

commit 3a2ec51e5bc35bcc27dd8740f02d8cdba0ee9b5b
Author: Floyd Butselaar <475474@student.saxion.nl>
Date:   Thu Feb 16 16:11:24 2023 +0100

    Documented the changes and issues that were made and encountered

commit 32d5b15233d6f0dbfd6026a9e74b7806d00a1942
Merge: 6c07dc0 1923492
Author: Floyd Butselaar <475474@student.saxion.nl>
Date:   Thu Feb 16 16:06:27 2023 +0100

    resolved merge conflicts

commit 6c07dc00cb394af2a962f80edd10de64be6a888c
Author: Floyd Butselaar <475474@student.saxion.nl>
Date:   Thu Feb 16 15:56:31 2023 +0100

    refactored the scraper

commit 1923492895e41f19318b1aa740ac511f3ecc58c9
Author: Floyd Butselaar <475474@student.saxion.nl>
Date:   Thu Feb 16 14:30:40 2023 +0000

    Update main.py

commit 6c29e272c6f674446d53cdf6dfb9f1577b4bfc39
Merge: b385704 e082e76
Author: Floyd Butselaar <475474@student.saxion.nl>
Date:   Thu Feb 16 15:27:49 2023 +0100

    pushed merge conflict and the resulting notes.txt

commit b385704b2e3e3df4c9c0c04770b5d962db196479
Author: Floyd Butselaar <475474@student.saxion.nl>
Date:   Thu Feb 16 15:22:21 2023 +0100

    title change

commit e082e7694c18b17015dc38901dfe8189fa642eb2
Merge: 535b737 14636f0
Author: Floyd Butselaar <475474@student.saxion.nl>
Date:   Thu Feb 16 14:19:48 2023 +0000

    Merge branch 'introduce_changes' into 'main'
    
    Changed readme title
    
    See merge request F.Butselaar/devops-blank-project!3

commit 14636f0e97936874cad68cdcf0505842e4aca62f
Author: Floyd Butselaar <475474@student.saxion.nl>
Date:   Thu Feb 16 14:18:35 2023 +0000

    Changed readme title

commit 535b7372b73b1f63495f83b9b124a01b87dfd683
Author: Floyd Butselaar <475474@student.saxion.nl>
Date:   Thu Feb 16 14:15:00 2023 +0000

    Update README.md with teachers note

commit 18c5b3b78565d22058318335f93975bd8c71d07f
Author: Floyd Butselaar <475474@student.saxion.nl>
Date:   Thu Feb 16 14:12:19 2023 +0000

    Update main.py

commit 43c05f6cffb47fbc6612da8516fed7812b55bb41
Merge: 17bcd50 b80fa03
Author: Cyn Dam-Schuit <501941@student.saxion.nl>
Date:   Thu Feb 16 14:03:14 2023 +0000

    Merge branch '1-export-to-csv' into 'main'
    
    added csv export
    
    Closes #1
    
    See merge request F.Butselaar/devops-blank-project!2

commit b80fa03cc06b2e587c36072429ae508b8791ed62
Author: Floyd Butselaar <475474@student.saxion.nl>
Date:   Thu Feb 16 15:01:33 2023 +0100

    added csv export

commit 17bcd5019d1f30e2558665635b45580d129cb7ce
Author: Floyd Butselaar <475474@student.saxion.nl>
Date:   Thu Feb 16 13:31:36 2023 +0100

    initial web scraper

commit e80ca0da5a9d21ad97af5d2bc8fd9c765b8931d1
Merge: a728c7a 7dbdd91
Author: F.Butselaar <475474@student.saxion.nl>
Date:   Tue Feb 14 19:17:53 2023 +0100

    resolved merge conflicts

commit a728c7aa39a02a7b4e6a14ce8fedefae6fc2ad98
Author: F.Butselaar <475474@student.saxion.nl>
Date:   Tue Feb 14 19:17:05 2023 +0100

    2.b Update README.md

commit 7dbdd911e06cace2d2571255327ca253b84a9db7
Author: Floyd Butselaar <475474@student.saxion.nl>
Date:   Tue Feb 14 18:15:50 2023 +0000

    Update README.md

commit f67acc2dd2c9e1815ecdce98993cc4413863777c
Author: F.Butselaar <475474@student.saxion.nl>
Date:   Tue Feb 14 19:14:42 2023 +0100

    1. Edit README.md

commit 335a25499fcffe733cda5581ac22b973e07d1113
Merge: 0f4f63f 3793e03
Author: Floyd Butselaar <475474@student.saxion.nl>
Date:   Tue Feb 14 18:12:33 2023 +0000

    Merge branch 'revert-0f4f63fe' into 'main'
    
    Revert "Initial commit"
    
    See merge request F.Butselaar/devops-blank-project!1

commit 3793e03c8e515ae3e3d5ff0b48078fa504dbb80f
Author: Floyd Butselaar <475474@student.saxion.nl>
Date:   Tue Feb 14 18:12:17 2023 +0000

    Revert "Initial commit"
    
    This reverts commit 0f4f63fe3a28894456f4b18eda60c1ac8a8c5087

commit 0f4f63fe3a28894456f4b18eda60c1ac8a8c5087
Author: F.Butselaar <475474@student.saxion.nl>
Date:   Tue Feb 14 19:11:15 2023 +0100

    Initial commit
