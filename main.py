import requests
import urllib.request
import time
import random
from bs4 import BeautifulSoup
from csv import export_csv



def main():
    data = []

    url = 'https://www.saxion.nl/studiegids/2022-2023/bachelor?program=1e54caf2-95c8-11e6-a8e9-9c1520524153/'
    response = requests.get(url)

    soup = BeautifulSoup(response.text, 'html.parser')

    tbodies = soup.findAll('tbody')
    for i in range(1, len(tbodies)):
        tbody = tbodies[i]
        trs = tbody.findAll('tr')
        for tr in trs:
            name = tr.findAll('td')[1].findAll('a')[0].text
            data.append(name)
            print(name)
    print(len(data))
    export_csv(data)
    return data

if __name__ == "__main__":
    main()