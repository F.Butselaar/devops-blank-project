from unicodedata import name
from datetime import datetime

def export_csv(data: list[str]):  
    with open(f'coursenames_{datetime.now().strftime("%d-%m-%Y %H_%M_%S")}.csv', 'a') as out:
        out.write('Name\n')
        for line in data:
            out.write(f'{line}\n')

if __name__ == "__main__":
    export_csv()